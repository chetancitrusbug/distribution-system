<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href='{!! asset('assets/images/favicon.png') !!}' rel='shortcut icon' type='image/png'>
	<title>@yield('title', trans('dashboard.label.home') ) | {{ config('app.name') }}</title>

	<!-- Global stylesheets -->
	@include('include.frontend.dashboard.cssfiles')

	<!-- /global stylesheets -->

    <!-- Js-->
    @include('include.frontend.dashboard.jsfiles')

</head>

<body>

	@include('include.frontend.dashboard.topnav')
    

	<!-- Page container -->
	<div class="page-container">

		<!-- Page content -->
		<div class="page-content">

			<!-- Main sidebar -->
			@include('include.frontend.dashboard.sidebar')
			<!-- /main sidebar -->


			<!-- Main content -->
			<div class="content-wrapper">
			
				<section class="top-section">
					<div class="page-header">
						<div class="page-header-content">
							<div class="row">
								<div class="col-lg-4 col-md-4 col-sm-12 clearfix">
									<div class="page-title">
										<h4><span class="text-semibold">Welcome</span> - User {{ @Auth::user()->name }}</h4>
									</div>
								</div>
								
							</div>
						</div>			
					</div><!-- /page header -->

				</section>
				@include('include.frontend.dashboard.page_notification')
				
				<!-- Content area -->
				<div class="content">
					@yield('content')
				</div>
				<!-- /content area -->
				<!-- Footer -->
				<div class="footer text-muted">
					&copy; <a href="#" target="_blank"> Distribution <?php echo date('Y');?> </a> all rights
				</div>
				<!-- /footer -->

			</div>
			<!-- /main content -->

		</div>
		<!-- /page content -->

	</div>
	<!-- /page container -->


</body>
</html>
@stack('js')
@stack('script-head')