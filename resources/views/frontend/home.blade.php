@extends('layouts.frontend')
@section('title','')

@section('content')
     @include('include.section.banner')
	 @include('include.section.howitwork')
	 @include('include.section.weatherfilter')
	 @include('include.section.weatherreport')
	 @include('include.section.weatheraction')
@endsection
