@extends('layouts.frontendDashboard')
@section('title','Sub Brand')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>Sub Brand</h1>
                </div>
            </div>
            <div class="box-content box-1">
                <div class="row btnbtmspace">
                    <div class="col-md-6">
                        <a href="{{ url('/subBrands/create') }}" class="btn btn-success btn-sm" title="Add Brand" >
                        <i class="fa fa-plus" aria-hidden="true"></i> Add Sub Brand </a>
                        
                    </div>
                </div>
                <div class="table-responsive" >
                    <table class="table" id="datatable-table">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Brand</th>
                                <th>Status</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@push('js')
<script>
    $(function() { 
        var url ="{{ url('/subBrands/') }}";
        var datatable = $('#datatable-table').DataTable({
            processing: true,
            serverSide: true,
            ajax: {
                    url: '{{ url('/subBrands-datatable/') }}',
                    type: "get", // method , by default get
                },
                columns: [
                    { data: 'brand_name',name:'brand_name',"searchable" : true}, 
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var parent_brand_id = '';
                            if(o.brand != null && o.brand.brand_name != '')
                            parent_brand_id = o.brand.brand_name;
                            else
                            parent_brand_id = '';
                            return parent_brand_id;
                        }
                    },
                    {
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var status = '';
                            if(o.status == '0')
                            status = '<a href="'+url+'/'+o.id+'?status=inactive" title="inactive"><button class="btn btn-danger btn-xs"><i class="fa fa-pencil-square-o" aria-hidden="true"></i> Inactive </button></a>';
                            else
                            status = "<a href='"+url+"/"+o.id+"?status=active' data-id="+o.id+" title='active'><button class='btn btn-success btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Active </button></a>";
                            return status;
                        }
                    }, 
                    { 
                        "data": null,
                        "searchable": false,
                        "orderable": false,
                        "render": function (o) {
                            var e="";var d="";
                                e= "<a href='"+url+"/"+o.id+"/edit' data-id="+o.id+"><button class='btn btn-primary btn-xs'><i class='fa fa-pencil-square-o' aria-hidden='true'></i> Edit </button></a>&nbsp;";
                                d = "<a href='javascript:void(0);' ><button class='btn btn-danger btn-xs del-item' data-id="+o.id+"><i class='fa fa-trash-o' aria-hidden='true'></i> Delete </button></a>&nbsp;";                    
                                return e+d;
                        }
                    }
                ]
        });
        
        $(document).on('click', '.del-item', function (e) {
            var id = $(this).attr('data-id');
            
            var url ="{{ url('/subBrands/') }}";
            url = url + "/" + id;
            var r = confirm("Are you sure you want to delete brand ?");
            if (r == true) {
                $.ajax({
                    type: "delete",
                    url: url ,
                    headers: {
                        "X-CSRF-TOKEN": "<?php echo csrf_token();?>"
                    },
                    success: function (data) {
                        datatable.draw();
                        toastr.success('Action Success!', data.message)
                    },
                    error: function (xhr, status, error) {
                        var erro = ajaxError(xhr, status, error);
                        toastr.error('Action Not Procede!',erro)
                    }
                });
            }
        });

    }); 
</script>
@endpush
