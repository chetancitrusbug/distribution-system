@extends('layouts.frontendDashboard')
@section('title','Station')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>Add Station</h1>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="box-content panel-body">
                <a href="{{ url('/station') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                
                 
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/station', 'class' => 'form-horizontal','id'=>'form','enctype' => 'multipart/data',]) !!}
                        @include ('frontend.station.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
