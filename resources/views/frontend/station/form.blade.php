<div class="form-group{{ $errors->has('name') ? ' has-error' : ''}}">
    {!! Form::label('name', 'Name' , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('name', null, ['class' => 'form-control']) !!}
        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('call_later') ? ' has-error' : ''}}">
    {!! Form::label('call_later', 'Call Later' , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('call_later', null, ['class' => 'form-control']) !!}
        {!! $errors->first('call_later', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('file') ? ' has-error' : ''}}">
    {!! Form::label('file', 'Upload Logo' , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::file('file', null, ['class' => 'form-control file-upload']) !!}
        {!! $errors->first('file', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('distribution_method') ? 'has-error' : ''}}">
    {!! Form::label('distribution_method', 'Distribution Method' , ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('distribution_method',['' =>'Select Method', '0'=> 'Email','1'=> 'FTP'] ,null, ['required' => 'required','class' => 'form-control']) !!} 
        {!! $errors->first('distribution_method','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group{{ $errors->has('distribution_email') ? ' has-error' : ''}}" id="emailSection">
    {!! Form::label('distribution_email', 'Call Later' , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('distribution_email', null, ['class' => 'form-control']) !!}
        {!! $errors->first('distribution_email', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div id="ftpSection">
    <div class="form-group{{ $errors->has('port') ? ' has-error' : ''}}" >
        {!! Form::label('port', 'FTP Port' , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('port', null, ['class' => 'form-control']) !!}
            {!! $errors->first('port', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('host') ? ' has-error' : ''}}" >
        {!! Form::label('host', 'FTP Host' , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('host', null, ['class' => 'form-control']) !!}
            {!! $errors->first('host', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('path') ? ' has-error' : ''}}" >
        {!! Form::label('path', 'FTP Path' , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('path', null, ['class' => 'form-control']) !!}
            {!! $errors->first('path', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('username') ? ' has-error' : ''}}" >
        {!! Form::label('username', 'FTP Username' , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('username', null, ['class' => 'form-control']) !!}
            {!! $errors->first('username', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

    <div class="form-group{{ $errors->has('password') ? ' has-error' : ''}}" >
        {!! Form::label('password', 'FTP Password' , ['class' => 'col-md-2 control-label']) !!}
        <div class="col-md-6">
            {!! Form::text('password', null, ['class' => 'form-control']) !!}
            {!! $errors->first('password', '<p class="help-block">:message</p>') !!}
        </div>
    </div>

</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status' , ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('status',['' =>'Select Status', '1'=> 'Active','0'=> 'Inactive'] ,null, ['required' => 'required','class' => 'form-control']) !!} 
        {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create' , ['class' => 'btn btn-primary btn-submit']) !!}
		<a href="{{ url('/station') }}" >
            <button type="button" class="btn btn-danger btn-cancel">Cancel</button>
        </a>
    </div>
</div>
@push('js')
<script>

$(document).ready(function() {
    $('#emailSection').hide();
	$('#ftpSection').hide();
    $("#distribution_method").change(function(){
        //console.log($(this).val());
		if($(this).val() == "1")
		{
			$('#emailSection').hide();
			$('#ftpSection').show();
		}
		else if($(this).val() == "0"){
			$('#emailSection').show();
			$('#ftpSection').hide();
		}
		else{
            $('#emailSection').hide();
			$('#ftpSection').hide();
		}
	
	});
    $("#distribution_method").trigger("change");
});
</script>
@endpush