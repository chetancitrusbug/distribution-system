@extends('layouts.frontendDashboard')
@section('title','Spots')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>Add Spots</h1>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="box-content panel-body">
                <a href="{{ url('/brands') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                   
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    
                    
                    {!! Form::open(['url' => '/store-spots', 'class' => 'form-horizontal','method'=>'POST','enctype' => 'multipart/data','files'=>'true']) !!}

                    <div class="form-group margin-tb">
                        {!! Form::label('file', 'Upload CSV File' , ['class' => 'col-md-2 control-label']) !!}
                        <div class="col-md-6">
                            {!! Form::file('file', null, ['class' => 'form-control file-upload', 'required' => 'required']) !!}
                        </div>
                    </div>
                    
                    
                    <div class="form-group {{ $errors->has('brand_id') ? 'has-error' : ''}}">
                        {!! Form::label('brand_id', 'Brand', ['class' => 'col-sm-2 control-label']) !!}
                        <div class="col-sm-6">
                                {!! Form::select('brand_id',$brand,null, ['class' => 'form-control','required' => 'required']) !!}
                                {!! $errors->first('brand_id','<p class="help-block with-errors">:message</p>') !!}
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-md-3">
                            {!! Form::submit('Upload', ['class' => 'btn btn-primary btn-submit']) !!}
                            <button class="btn btn-danger btn-cancel">Cancel</button>
                        </div>
                    </div>
                
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
