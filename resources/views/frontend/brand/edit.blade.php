@extends('layouts.frontendDashboard')
@section('title','Brand')
@section('content')
    <div class="row">

        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading"> <h3>Edit Brand</h3> </div>
                <div class="panel-body">
                    <a href="{{ url('/brands') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                    <br />
                    <br />
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif

                    {!! Form::model($brand, [
                        'method' => 'PATCH',
                        'url' => ['/brands', $brand->id],
                        'class' => 'form-horizontal',
                        'id'=>'form',
                        'enctype'=>'multipart/form-data'
                    ]) !!}


                    @include ('frontend.brand.form', ['submitButtonText' => 'Update'])

                    {!! Form::close() !!}

                </div>
            </div>
        </div>
    </div>
@endsection
