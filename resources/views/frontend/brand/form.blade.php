<div class="form-group{{ $errors->has('brand_name') ? ' has-error' : ''}}">
    {!! Form::label('brand_name', 'Brand Name' , ['class' => 'col-md-2 control-label']) !!}
    <div class="col-md-6">
        {!! Form::text('brand_name', null, ['class' => 'form-control', 'required' => 'required']) !!}
        {!! $errors->first('brand_name', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('status') ? 'has-error' : ''}}">
    {!! Form::label('status', 'Status' , ['class' => 'col-sm-2 control-label']) !!}
    <div class="col-sm-6">
        {!! Form::select('status',['' =>'Select Status', '1'=> 'Active','0'=> 'Inactive'] ,null, ['required' => 'required','class' => 'form-control']) !!} 
        {!! $errors->first('status','<p class="help-block with-errors">:message</p>') !!}
    </div>
</div>

<div class="form-group">
    <div class="col-md-offset-2 col-md-4">
        {!! Form::submit(isset($submitButtonText) ? $submitButtonText : 'Create' , ['class' => 'btn btn-primary btn-submit']) !!}
		<a href="{{ url('/brands') }}" >
            <button type="button" class="btn btn-danger btn-cancel">Cancel</button>
        </a>
    </div>
</div>
@push('js')
<script>

</script>
@endpush