@extends('layouts.frontendDashboard')
@section('title','Brand')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box bordered-box blue-border">
            <div class="box-header blue-background">
                <div class="title">
                    <h1>Add Brand</h1>
                </div>
                <div class="actions">
                    
                </div>
            </div>
            <div class="box-content panel-body">
                <a href="{{ url('/brands') }}" title="Back"><button class="btn btn-warning btn-sm"><i class="fa fa-arrow-left" aria-hidden="true"></i> Back </button></a>
                
                 
                    @if ($errors->any())
                        <ul class="alert alert-danger">
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    @endif
                    {!! Form::open(['url' => '/brands', 'class' => 'form-horizontal','id'=>'form']) !!}
                        @include ('frontend.brand.form')
                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
