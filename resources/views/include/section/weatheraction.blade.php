<section class="weather-action-container clearfix">
    <div class="container">
        <div class="row">
            <div class="weather-action-div clearfix">
                <ul>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-1.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Increase online conversions & ROI</span>
                        </div>
                    </li>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-2.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Automate your adverts with the weather</span>
                        </div>
                    </li>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-3.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Reach the audiences you care about with better targeting</span>
                        </div>
                    </li>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-4.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Performance and intelligence reporting</span>
                        </div>
                    </li>
                    <li class="left">
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-5.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Low subscription fees to support small and start up businesses</span>
                        </div>
                    </li>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-6.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Avoid running campaigns in weather conditions not benefiting your product or service</span>
                        </div>
                    </li>
                    <li>
                        <div class="weather-box-div">
                            <div class="thumb"><img src="{!! asset('/frontend/images/img-7.png') !!}" alt="" class="img-responsive m-center"></</div>
                            <span>Ensure you get the best return for your advertising spend</span>
                        </div>
                    </li>
                </ul>
            </div><!-- end of weather-action-div -->            
        </div>
    </div>
</section><!-- end of weather-action-container -->  