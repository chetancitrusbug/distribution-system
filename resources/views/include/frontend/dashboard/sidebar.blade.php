<!-- Main sidebar -->
<div class="sidebar sidebar-main">
	<div class="sidebar-content">
		<!-- Main navigation -->
		<div class="sidebar-category sidebar-category-visible">
			<div class="category-content no-padding">
				<ul class="navigation navigation-main navigation-accordion">

					<!-- Main -->
					
					<li class="active"><a href="{{ url('/dashboard') }}"><i class="fas fa-home"></i> <span>Dashboard</span></a></li>
					<li>
						<a href="{{ url('/brands') }}"><i class="fab fa-cc-stripe"></i><span>Brand</span></a>
						<ul>
							<li><a href="{{ url('/brands/create') }}"><i class="fa fa-plus"></i> Add</a></li>
							<li><a href="{{ url('/brands') }}"><i class="icon-clipboard3"></i> List</a></li>
							<li><a href="{{ url('/addSpots') }}"><i class="fa fa-plus"></i> Add Spots</a></li>
						</ul>
					</li>
					<li>
						<a href="{{ url('/subBrands') }}"><i class="fab fa-cc-stripe"></i><span>Sub Brand</span></a>
						<ul>
								<li><a href="{{ url('/subBrands/create') }}"><i class="fa fa-plus"></i> Add</a></li>
							<li><a href="{{ url('/subBrands') }}"><i class="icon-clipboard3"></i> List</a></li>
						</ul>
					</li>
					<li>
						<a href="{{ url('/playList') }}"><i class="fab fa-cc-stripe"></i><span>Play List</span></a>
						<ul>
							<li><a href="{{ url('/playList/create') }}"><i class="fa fa-plus"></i> Add</a></li>
							<li><a href="{{ url('/playList') }}"><i class="icon-clipboard3"></i> List</a></li>
							
						</ul>
					</li>

					<li>
						<a href="{{ url('/station') }}"><i class="fab fa-cc-stripe"></i><span>Station</span></a>
						<ul>
							<li><a href="{{ url('/station/create') }}"><i class="fa fa-plus"></i> Add</a></li>
							<li><a href="{{ url('/station') }}"><i class="icon-clipboard3"></i> List</a></li>
							
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /main navigation -->
		
	</div>
</div>
<!-- /main sidebar -->