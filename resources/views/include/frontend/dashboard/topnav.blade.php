    <!-- Main navbar -->
	<div class="navbar navbar-default header-highlight">
			<div class="navbar-header">
				<a class="navbar-brand" href="{{ url('/dashboard') }}"><img src="assets/images/logo.png" alt="" width="150px" height="200px"></a>
	 
				<ul class="nav navbar-nav visible-xs-block">
					<li><a data-toggle="collapse" data-target="#navbar-mobile"><i class="icon-tree5"></i></a></li>
					<li><a class="sidebar-mobile-main-toggle"><i class="icon-paragraph-justify3"></i></a></li>
				</ul>
			</div>
	
			<div class="navbar-collapse collapse" id="navbar-mobile">
				<ul class="nav navbar-nav">
					<li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
					</li>
				</ul>
					
				
				<ul class="nav navbar-nav navbar-right">
					 
					
					<!--<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown">
							<i class="icon-gear position-left"></i>
							Settings
							<span class="caret"></span>
						</a>
	
						<ul class="dropdown-menu dropdown-menu-right">
							
						</ul>
					</li> start setting -->
	
					<li class="dropdown dropdown-user">
						<a class="dropdown-toggle" data-toggle="dropdown">
							<img src="assets/images/placeholder.jpg" alt="">
							<span>{{ @Auth::user()->name }}</span>
							<i class="caret"></i>
						</a>
	
						<ul class="dropdown-menu dropdown-menu-right">
							<!--<li><a href="{{ url('profile/edit') }}"><i class="fa fa-wrench icon-push"> </i> Edit Profile</a></li>-->
							<li><a href="{{ url('profile/change-password') }}"><i class="fa fa-lock icon-push"> </i> Change Password</a></li>
							<li><a href="{{ url('/profile') }}"><i class="icon-user-plus"></i> Profile </a></li>
							<li class="divider"></li>
							<li><a href="{{ url('/logout') }}"
									onclick="event.preventDefault();
									document.getElementById('logout-form').submit();">
									<i class="icon-switch2"></i> Logout</a>
									<form id="logout-form" action="{{ url('/logout') }}" method="POST"  style="display: none;">
												 {{ csrf_field() }}
									</form>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<!-- /main navbar -->
	