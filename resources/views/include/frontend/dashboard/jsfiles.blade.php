    
    
    
    
    <!-- Core JS files -->
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/loaders/pace.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/js/jquery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/js/bootstrap.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/loaders/blockui.min.js') !!}"></script>
    
	<!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/visualization/d3/d3.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/visualization/d3/d3_tooltip.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/forms/styling/switchery.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/forms/styling/uniform.min.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/plugins/forms/selects/bootstrap_multiselect.js') !!}"></script>
    <script type="text/javascript" src="{!! asset('/frontend/dashboard/js/core/app.js') !!}"></script>
    
    <!-- /theme JS files -->
    
    {{-- Data Table--}}
    <script src="{!! asset('assets/javascripts/datatable/jquery.dataTables.min.js')!!}"></script>
    <script src="{!! asset('assets/javascripts/datatable/dataTables.responsive.min.js')!!}"></script>
       {{-- toastr --}}
    <script src="{!! asset('assets/javascripts/toastr.min.js')!!}"></script>
