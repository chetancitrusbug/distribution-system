<header>
    <div class="header-div">

        <div class="header-top-div clearfix">
                <div class="logo-div clearfix">
                        <a href="index.html"><img src="{!! asset('/frontend/images/logo.png') !!}" alt="logo" class="img-responsive" /></a>
                    </div><!-- end of logo-div -->
                    <div class="nav-div clearfix">
                        <nav class="navbar clearfix">
                            
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header clearfix">
                                <button type="button" class="navbar-toggle collapsed clearfix" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                                    
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse clearfix" id="bs-example-navbar-collapse-1">
                                <ul class="nav navbar-nav clearfix">
                                    <li><a href="learn-more.html">Learn More</a></li>
                                    <li><a href="plan.html">Prices</a></li>
                                    <li><a href="support.html">Support</a></li>
									<li class="desktop-hidden "><a href="sign-up.html">Sign Up</a></li>
									<li class="desktop-hidden "><a href="login.html">Log In</a></li>
                                </ul>
                            </div><!-- /.navbar-collapse -->
                            
                            
                        </nav>
                    </div><!-- end of nav-div -->

                    <div class="top-strip-div clearfix mobile-hidden">
                        <span><a href="sign-up.html" class="singup-link">Sign Up</a></span>
                        <span><a href="login.html" class="login-link">Log In</a></span>
                    </div><!-- end of top strip -->
                        

        </div>



    </div>
</header>