<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Station extends Model
{
    use SoftDeletes;
    protected $table = 'station';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name','call_later','logo','distribution_method','distribution_email','created_by', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
