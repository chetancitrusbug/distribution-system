<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class PlayList extends Model
{
    use SoftDeletes;
    protected $table = 'playlist';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['playlist_name', 'share_playlist', 'status' , 'created_by' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    
}
