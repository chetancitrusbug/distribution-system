<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpotType extends Model
{
    use SoftDeletes;
    protected $table = 'spot_type';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['spot', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
