<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class SpotBrand extends Model
{
    use SoftDeletes;
    protected $table = 'spot_brand';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_id','spot_id','require_approval','brand_apporoval_ids','comment','created_by', 'status'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
