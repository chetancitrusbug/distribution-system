<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SpotType;
use App\SpotBrand;
use App\Brand;
use Session;
use Auth;
use Yajra\Datatables\Datatables;

class BrandController extends Controller
{
    public function addSpots(Request $request)
    {
       
        $brand = brand::where('parent_brand_id','0')->where('status','1')->pluck('brand_name','id')->prepend('Select Brand','');
        
        return view('frontend.brand.addBulkSpots',compact('sample','brand'));
    }
    public function storeSpots(Request $request)
    {
        $requestdata = $request->all();
        $this->validate($request, [
            'brand_id' => 'required',
            'file' => 'required',
        ]);
        
        $data = array(
            'status' => 1,
        );

        $file = $request->file('file');
        if(!empty($file)){
            $data['spot'] = str_replace(' ','',$file->getClientOriginalName());
            $path = public_path('/uploads/spots').'/'.$request->brand_id;
            $file->move($path,$data['spot']);
            $filePath = public_path('/uploads/spots').'/'.$request->brand_id.'/'.$data['spot'];
        }
        else
        {
            $data['spot'] = '';
        }
        $SpotType = SpotType::create($data);

        $data = array(
            'brand_id' => $request->input('brand_id'),
            'spot_id' => $SpotType->id,
            'require_approval' => 0,
            'brand_apporoval_ids' => 0,
            'comment' => '',
            'status' => 1,
            'created_by' => 1
        );
        
        $SpotBrand = SpotBrand::create($data);
        Session::flash('flash_success', "Added Spot");
        return redirect('addSpots');
    }

    public function index(Request $request)
    {
        return view('frontend.brand.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $brand = Brand::where('parent_brand_id','0');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(brand.brand_name LIKE  '%$value%' )";
                $brand = $brand->whereRaw($where_filter);
            }
        }
        $brand = $brand->orderBy('brand.id', 'desc')->get();

        return Datatables::of($brand)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('frontend.brand.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestdata = $request->all();
        $this->validate($request, [
            'brand_name' => 'required',
        ]);
        
        $data = array(
            'brand_name' => $request->input('brand_name'),
            'parent_brand_id' => 0,
            'created_by' => (isset(Auth::user()->id) ? Auth::user()->id : 1),
            'status' => $request->input('status')
        );
        $data['status']  = (($data['status'] != '') ? $data['status'] : 1);
        
        $brand = brand::create($data);

        return redirect('/brands')->with('flash_success', 'Brand added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $brand = brand::where('parent_brand_id','0')->where('id', $id)->first();
        if ($brand) {
            return view('frontend.brand.edit', compact('brand'));
        } else {
            return redirect('/brands')->with('flash_error', 'Brand is not exist!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'brand_name' => 'required',
        ]);
        $requestData = array(
            'brand_name' => $request->input('brand_name'),
            'status' => $request->input('status'));
        
        $brand = brand::where('id', $id);
        $brand->update($requestData);
        return redirect('/brands')->with('flash_success', 'Brand Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $brand = brand::where('id', $id);
        $brand->delete();

        $message='Brand Deleted';
        return response()->json(['message'=>$message],200);
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $brand = brand::where('id', $id)->first();
        if($brand == NULL) {
            return redirect('/brands')->with('flash_error', 'Brand is not exist!');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $brand->status= '0';
                $brand->update();        
            }else{
                $brand->status= '1';
                $brand->update();             
            }
            return redirect('/brands')->with('flash_success', 'Brand Status Updated Successfully');
        }
        
        return redirect('/brands');
    }

    
    
}
