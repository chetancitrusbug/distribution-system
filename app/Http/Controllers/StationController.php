<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Station;
use App\StationDistributionFtp;
use Session;
use Auth;
use Yajra\Datatables\Datatables;

class StationController extends Controller
{
    
    public function index(Request $request)
    {
        return view('frontend.station.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $station = Station::orderBy('station.id', 'desc');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(station.name LIKE  '%$value%' )";
                $station = $station->whereRaw($where_filter);
            }
        }
        $station = $station->get();

        return Datatables::of($station)
            ->make(true);
        exit;
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('frontend.station.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestdata = $request->all();
        //dd($requestdata);
        $rules = array(
            'name' => 'required',
            'call_later' => 'required',
            'file' => 'required',
            'distribution_method' => 'required',
            'status' => 'required',
        );
        if($request->distribution_method != '')
        {
            if($request->distribution_method == 0)
            {
                $rules['distribution_email'] = 'required|email';
            }
            else
            {
                $rules['port'] = 'required';
                $rules['host'] = 'required';
                $rules['path'] = 'required';
                $rules['username'] = 'required';
                $rules['password'] = 'required';
            }
        }
        $this->validate($request, $rules);
        $data = array(
            'name' => $request->input('name'),
            'call_later' => $request->input('call_later'),
            'distribution_method' => $request->input('distribution_method'),
            'created_by' => (isset(Auth::user()->id) ? Auth::user()->id : 1),
            'status' => $request->input('status')
        );

        if($request->distribution_method == 0)
        {
            $data['distribution_email']  = $request->input('distribution_email');
        }
        else{
            $data['distribution_email']  = '';
        }

        $data['status']  = (($data['status'] != '') ? $data['status'] : 1);
        $file = $request->file('file');
        if(!empty($file)){
            $logoName = str_replace(' ','',$file->getClientOriginalName());
            $path = public_path('/uploads/station');
            $file->move($path,$logoName);
            $data['logo'] = public_path('/uploads/station').'/'.$logoName;
        }
        else
        {
            $data['logo'] = '';
        }
        $station = Station::create($data);
        
        if($request->distribution_method != 0)
        {
            $data = array();
            $data['station_id'] = $station->id;
            $data['port'] = $request->port;
            $data['host'] = $request->host;
            $data['path'] = $request->path;
            $data['username'] = $request->username;
            $data['password'] = $request->password;
            $StationDistributionFtp = StationDistributionFtp::create($data);
        }
        
        return redirect('/station')->with('flash_success', 'Station added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $station = Station::where('id', $id)->first();
        if ($station) {
            return view('frontend.station.edit', compact('station'));
        } else {
            return redirect('/stations')->with('flash_error', 'Station is not exist!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'station_name' => 'required',
        ]);
        $requestData = array(
            'station_name' => $request->input('station_name'),
            'status' => $request->input('status'));
        
        $station = Station::where('id', $id);
        $station->update($requestData);
        return redirect('/station')->with('flash_success', 'Station Updated Successfully!');
    }

    public function destroy(Request $request, $id)
    {
        $station = Station::where('id', $id);
        $station->delete();

        $message='station Deleted';
        return response()->json(['message'=>$message],200);
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $station = Station::where('id', $id)->first();
        if($station == NULL) {
            return redirect('/station')->with('flash_error', 'Station is not exist!');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $station->status= '0';
                $station->update();        
            }else{
                $station->status= '1';
                $station->update();             
            }
            return redirect('/station')->with('flash_success', 'Station Status Updated Successfully');
        }
        
        return redirect('/station');
    }

    
    
}
