<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Brand;
use Session;
use Auth;
use Yajra\Datatables\Datatables;

class SubBrandController extends Controller
{
    public function index(Request $request)
    {
        return view('frontend.subBrand.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $brand = Brand::with('brand')->where('parent_brand_id','!=','0');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(brand.brand_name LIKE  '%$value%' )";
                $brand = $brand->whereRaw($where_filter);
            }
        }
        $brand = $brand->orderBy('brand.id', 'desc')->get();

        return Datatables::of($brand)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        $brand = brand::where('parent_brand_id','0')->where('status','1')->pluck('brand_name','id')->prepend('Select Brand','');
        return view('frontend.subBrand.create',compact('brand'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestdata = $request->all();
        $this->validate($request, [
            'brand_name' => 'required',
            'parent_brand_id' => 'required',
        ]);
        
        $data = array(
            'brand_name' => $request->input('brand_name'),
            'parent_brand_id' => $request->input('parent_brand_id'),
            'created_by' => (isset(Auth::user()->id) ? Auth::user()->id : 1),
            'status' => $request->input('status')
        );
        $data['status']  = (($data['status'] != '') ? $data['status'] : 1);
        
        $brand = brand::create($data);

        return redirect('/subBrands')->with('flash_success', 'Brand added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $subbrand = brand::where('parent_brand_id','!=','0')->where('id', $id)->first();
        if ($subbrand) {
            $brand = brand::where('parent_brand_id','0')->where('status','1')->pluck('brand_name','id')->prepend('Select Brand','');
            return view('frontend.subBrand.edit', compact('subbrand','brand'));
        } else {
            return redirect('/subBrands')->with('flash_error', 'Brand is not exist!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'brand_name' => 'required',
            'parent_brand_id' => 'required',
        ]);
        $requestData = array(
            'brand_name' => $request->input('brand_name'),
            'parent_brand_id' => $request->input('parent_brand_id'),
            'status' => $request->input('status'));
        
        $brand = brand::where('id', $id);
        $brand->update($requestData);
        return redirect('/subBrands')->with('flash_success', 'Brand Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $brand = brand::where('id', $id);
        $brand->delete();

        $message='Brand Deleted';
        return response()->json(['message'=>$message],200);
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $brand = brand::where('id', $id)->first();
        if($brand == NULL) {
            return redirect('/subBrands')->with('flash_error', 'Brand is not exist!');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $brand->status= '0';
                $brand->update();        
            }else{
                $brand->status= '1';
                $brand->update();             
            }
            return redirect('/subBrands')->with('flash_success', 'Brand Status Updated Successfully');
        }
        
        return redirect('/subBrands');
    }
}
