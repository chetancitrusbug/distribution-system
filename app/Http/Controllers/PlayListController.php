<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PlayList;
use Session;
use Auth;
use Yajra\Datatables\Datatables;

class PlayListController extends Controller
{
    public function index(Request $request)
    {
        return view('frontend.playList.index');
    }

    /**
     * Display datatable value
     *
     * @return void
     */
    public function datatable(request $request)
    {
        $playList = PlayList::orderBy('playlist.id', 'desc');
        if ($request->has('search') && $request->get('search') != '') {
            $search = $request->get('search');
            if ($search['value'] != '') {
                $value = $search['value'];
                $where_filter = "(playlist.playlist_name LIKE  '%$value%' )";
                $playList = $playList->whereRaw($where_filter);
            }
        }
        $playList = $playList->get();

        return Datatables::of($playList)
            ->make(true);
        exit;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create(Request $request)
    {
        return view('frontend.playList.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function store(Request $request)
    {
        $requestdata = $request->all();
        $this->validate($request, [
            'playList_name' => 'required',
        ]);
        
        $data = array(
            'playList_name' => $request->input('playList_name'),
            'parent_playList_id' => 0,
            'created_by' => (isset(Auth::user()->id) ? Auth::user()->id : 1),
            'status' => $request->input('status')
        );
        $data['status']  = (($data['status'] != '') ? $data['status'] : 1);
        
        $playList = PlayList::create($data);

        return redirect('/playLists')->with('flash_success', 'playList added!');
    }
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */

    public function edit(Request $request, $id)
    {
        $playList = PlayList::where('parent_playList_id','0')->where('id', $id)->first();
        if ($playList) {
            return view('frontend.playList.edit', compact('playList'));
        } else {
            return redirect('/playLists')->with('flash_error', 'playList is not exist!');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @param  \Illuminate\Http\Request $request
     *
     * @return void
     */
    public function update($id, Request $request)
    {
        $this->validate($request, [
            'playList_name' => 'required',
        ]);
        $requestData = array(
            'playList_name' => $request->input('playList_name'),
            'status' => $request->input('status'));
        
        $playList = PlayList::where('id', $id);
        $playList->update($requestData);
        return redirect('/playLists')->with('flash_success', 'playList Updated Successfully!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     *
     * @return void
     */
    
    public function destroy(Request $request, $id)
    {
        $playList = PlayList::where('id', $id);
        $playList->delete();

        $message='playList Deleted';
        return response()->json(['message'=>$message],200);
    }

   
    /**
     * Display the specified resource.
     *
     * @param  int $id
     *
     * @return void
     */
    public function show(Request $request,$id)
    {   

        $playList = PlayList::where('id', $id)->first();
        if($playList == NULL) {
            return redirect('/playList')->with('flash_error', 'Play List is not exist!');
        }
        //change client status
        $status = $request->get('status');
        if(!empty($status)){
            if($status == 'active' ){
                $playList->status= '0';
                $playList->update();        
            }else{
                $playList->status= '1';
                $playList->update();             
            }
            return redirect('/playList')->with('flash_success', 'Play List Status Updated Successfully');
        }
        
        return redirect('/playList');
    }
}
