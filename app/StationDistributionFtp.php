<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StationDistributionFtp extends Model
{
    use SoftDeletes;
    protected $table = 'station_distribution_ftp';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['station_id','port','host','path','username','password'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
}
