<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Brand extends Model
{
    use SoftDeletes;
    protected $table = 'brand';  

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['brand_name', 'parent_brand_id', 'status' , 'created_by' ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */

    
    public function brand(){
        return $this->belongsTo('App\Brand','parent_brand_id','id')->select( 'id', 'brand_name');
    }
}
