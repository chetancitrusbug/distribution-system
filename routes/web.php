<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Auth::routes();

//Route::get('/', 'HomeController@index');
Route::get('/', 'HomeController@dashboard');
Route::resource('/brands', 'BrandController');
Route::get('/addSpots', 'BrandController@addSpots');
Route::post('/store-spots', 'BrandController@storeSpots');
Route::get('/brands-datatable', 'BrandController@datatable');


Route::resource('/subBrands', 'SubBrandController');
Route::get('/subBrands-datatable', 'SubBrandController@datatable');

Route::resource('/playList', 'PlayListController');
Route::get('/playList-datatable', 'PlayListController@datatable');

Route::resource('/station', 'StationController');
Route::get('/station-datatable', 'StationController@datatable');

Route::group(['prefix' => 'admin','middleware' => ['auth', 'admin']], function () {
	
	Route::get('/', 'Admin\AdminController@index');
	Route::resource('roles', 'Admin\RolesController');
	Route::resource('permissions', 'Admin\PermissionsController');
	Route::resource('users', 'Admin\UsersController');
	
	Route::get('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@getGenerator']);
	Route::post('generator', ['uses' => '\Appzcoder\LaravelAdmin\Controllers\ProcessController@postGenerator']);

});