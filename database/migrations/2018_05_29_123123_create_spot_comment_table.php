<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSpotCommentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('spot_comment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('spot_brand_id')->nullable();
            $table->string('comment')->nullable();
            $table->datetime('mark')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('upload')->nullable();
            $table->integer('actions')->nullable();
            $table->integer('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('spot_comment');
    }
}
