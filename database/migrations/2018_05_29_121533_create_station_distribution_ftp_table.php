<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStationDistributionFtpTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('station_distribution_ftp', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('station_id')->nullable();
            $table->string('port')->nullable();
            $table->string('host')->nullable();
            $table->string('path')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('station_distribution_ftp');
    }
}
