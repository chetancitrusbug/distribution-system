<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBrandFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('brand_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('file_type_id')->nullable();
            $table->integer('brand_id')->nullable();
            $table->string('file_name')->nullable();
            $table->integer('spot_id')->nullable();
            $table->string('unique_idedintity')->nullable();
            $table->integer('status')->default(1);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('brand_file');
    }
}
