<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOneTimeStationTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('one_time_station', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name')->nullable();
            $table->integer('method')->nullable();
            $table->string('email')->nullable();
            $table->string('server')->nullable();
            $table->string('pathinfo')->nullable();
            $table->string('username')->nullable();
            $table->string('password')->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('one_time_station');
    }
}
